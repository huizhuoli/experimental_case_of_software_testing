package cn.swust.functest.moneyjudge;

import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.DropMode;
import javax.swing.SwingConstants;

import cn.swust.functest.RegularTextField;
 

/**
 * @author 阿文
 * 进行找零的程序：可以清晰分辨出需要找出的最少的钞票数
 * 1.金额不足将不显示，并且会倒贴。
 * 2.金额刚刚好的时候，不会出现0张钱的数字。
 * 3.不存在输入错误。
 * 4.没有金额大小的控制条件。（可以超过一百小于1一百）
 */
public class ChangeMoneyFrameV1 extends JFrame{
	
	private String newFifty;//以下四个是用于来表示最后结果（几张）的变量
	private String newTen;
	private String newFive;
	private String newOne;
	private static final long serialVersionUID = 1L;
	/**
	 * 表示支付金额
	 */
	RegularTextField moneySide;
	/**
	 * 表示商品的金额
	 */
	RegularTextField priceSide;
	
	JLabel result,Fifty,Ten,Five,One;//关于结果的显现
	
	public ChangeMoneyFrameV1(){
		 
		setTitle("功能测试1_收款");
		getContentPane().setLayout(null);// 在标题显现
		
		JLabel label = new JLabel("功能测试4—找零");
		label.setForeground(Color.BLUE);
		label.setBounds(10, 31, 200, 18);
		getContentPane().add(label);//frame 是个窗口, getContentPane是获取窗口的面板, 然后把 label add到面板中
		
		JLabel label_1 = new JLabel("V1");
		label_1.setForeground(Color.RED);
		label_1.setFont(new Font("Arial", Font.BOLD, 22));
		label_1.setBounds(250, 28, 72, 18);
		getContentPane().add(label_1);
		
		JLabel lblA = new JLabel("商品金额输入：");
		lblA.setBounds(40, 114, 151, 18);
		getContentPane().add(lblA);
		 
		JLabel lblF = new JLabel("金额输入：");
		lblF.setBounds(40, 65, 99, 18);
		getContentPane().add(lblF);
		
		JLabel lblB = new JLabel("五十元：");
		lblB.setBounds(10, 172, 64, 18);
		getContentPane().add(lblB);
		
		JLabel lblC = new JLabel("十元：");
		lblC.setBounds(84, 172, 54, 18);
		getContentPane().add(lblC);
		
		JLabel lblD = new JLabel("五元：");
		lblD.setBounds(148, 172, 54, 18);
		getContentPane().add(lblD);
		
		JLabel lblE = new JLabel("一元：");
		lblE.setBounds(212, 172, 54, 18);
		getContentPane().add(lblE);
		//《《《《《《《
		moneySide = new RegularTextField();
		moneySide.setHorizontalAlignment(SwingConstants.LEFT);
		
		moneySide.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					priceSide.requestFocusInWindow();
				} 
			}
		});
		moneySide.setRegularText("");
		moneySide.setName("moneySide");
		moneySide.setColumns(10);
		moneySide.setBounds(131, 63, 93, 24);
		getContentPane().add(moneySide);
		//《《《《《《
		priceSide = new RegularTextField();
		priceSide.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					moneySide.requestFocusInWindow();
				} 
			}
		});
		
		priceSide.setRegularText("");
		priceSide.setName("priceSide");
		priceSide.setColumns(10);
		priceSide.setBounds(131, 112, 93, 24);
		getContentPane().add(priceSide);
		//《《《《《《《《
		JButton judgeBtn = new JButton("开始");
		judgeBtn.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
			}
		});
		judgeBtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				showResult();				
			}
		});
		judgeBtn.setName("judgeBtn");
		judgeBtn.setBounds(271, 83, 93, 27);
		getContentPane().add(judgeBtn);
		//《《《《《《按键
		result = new JLabel("结果：");
		result.setVisible(false);
		result.setName("result");
		result.setDisplayedMnemonic(KeyEvent.VK_ENTER);
		result.setBounds(10, 156, 129, 18);
		getContentPane().add(result);
		
		Fifty = new JLabel("五十元：");
		Fifty.setVisible(false);
		Fifty.setName("number");
		Fifty.setDisplayedMnemonic(KeyEvent.VK_ENTER);
		Fifty.setBounds(10, 172, 64, 18);
		getContentPane().add(Fifty);
		
		Ten = new JLabel("十元：");
		Ten.setVisible(false);
		Ten.setName("number");
		Ten.setDisplayedMnemonic(KeyEvent.VK_ENTER);
		Ten.setBounds(84, 172, 54, 18);
		getContentPane().add(Ten);
		
		Five = new JLabel("五元：");
		Five.setVisible(false);
		Five.setName("number");
		Five.setDisplayedMnemonic(KeyEvent.VK_ENTER);
		Five.setBounds(148, 172, 54, 18);
		getContentPane().add(Five);
		
		One = new JLabel("一元：");
		One.setVisible(false);
		One.setName("number");
		One.setDisplayedMnemonic(KeyEvent.VK_ENTER);
		One.setBounds(212, 172, 54, 18);
		getContentPane().add(One);
		
		JLabel NewLabel1 = new JLabel("元");
		NewLabel1.setBounds(234, 114, 32, 18);
		getContentPane().add(NewLabel1);
		
		JLabel NewLabel2 = new JLabel("元");
		NewLabel2.setBounds(234, 65, 32, 18);
		getContentPane().add(NewLabel2);
	}

 
	public interface moneyType{//接口
	    int unenough = 0;
		int exist = 2;
		int justenough = 3;
	}
	
	public int check() {
		int money, price, different;
		int fifty = 0, ten = 0, five = 0, one = 0;
			money = Integer.parseInt(moneySide.getText());// 从区域中拿去数值
			price = Integer.parseInt(priceSide.getText());
		different = money - price;
		if (different == 0) {
		return moneyType.justenough;
		} 
		else {
			fifty = different / 50;
			different = different - (50 * fifty);
			ten = different / 10;
			different = different - (10 * ten);
			five = different / 5;
			different = different - (5 * five);
			one = different / 1;
			newFifty = String.valueOf(fifty);
			newTen = String.valueOf(ten);
			newFive = String.valueOf(five);
			newOne = String.valueOf(one);
			return moneyType.exist;
		}
	}
	
	private void showResult()
	{
		int iresult = check();//通过这个函数来判断它的实际类型
		switch (iresult)
		{//通过接口来判别类型
		case moneyType.justenough:
			result.setText("结果：金额刚刚好!");//显示窗口
			result.setForeground(Color.BLUE);//设定颜色部分
			result.setVisible(true);//显现窗口
			break;	
		case moneyType.exist:
			result.setText("结果：找零如下!");
			result.setForeground(Color.BLUE);
			result.setVisible(true);//显现窗口
			Fifty.setText("五十元   "+newFifty);
			Fifty.setForeground(Color.black);
			Fifty.setVisible(true);//显现窗口
			Ten.setText("十元   "+newTen);
			Ten.setForeground(Color.black);
			Ten.setVisible(true);//显现窗口
			Five.setText("五元   "+newFive);
			Five.setForeground(Color.black);
			Five.setVisible(true);//显现窗口
			One.setText("一元   "+newOne);
			One.setForeground(Color.black);
			One.setVisible(true);//显现窗口
			break;

		}
	}
	
	public static void main(String[] args) { 
		ChangeMoneyFrameV1 frame = new ChangeMoneyFrameV1();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(650, 250);
		frame.setVisible(true); 
	}
}
