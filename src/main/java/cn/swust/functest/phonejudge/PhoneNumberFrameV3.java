package cn.swust.functest.phonejudge;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;

import cn.swust.functest.RegularTextField;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.Font; 
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * @author 阿文
 *能够判断电话号码的标准
 */
public class PhoneNumberFrameV3 extends JFrame {
	 
	private static final long serialVersionUID = 1L;
	private RegularTextField areaCode;
	private JLabel lblNewLabel_1;
	private RegularTextField prefix;
	private RegularTextField suffix;
	JLabel result;
	
	 
	public PhoneNumberFrameV3() {
		addWindowListener(new WindowAdapter() {
			@Override
			public void windowClosed(WindowEvent arg0) {
				setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			}
		});
		setTitle("功能测试1_电话号码问题");
		getContentPane().setLayout(null);
		
		result = new JLabel("结果：");
		result.setVisible(false);
		result.setName("result");
		result.setDisplayedMnemonic(KeyEvent.VK_ENTER);
		result.setBounds(163, 164, 129, 18);
		getContentPane().add(result);

		JLabel label = new JLabel("地区码：");
		label.setBounds(86, 42, 72, 18);
		getContentPane().add(label);

		JLabel lblNewLabel = new JLabel("前  缀：");
		lblNewLabel.setBounds(86, 87, 72, 18);
		getContentPane().add(lblNewLabel);

		lblNewLabel_1 = new JLabel("后  缀：");
		lblNewLabel_1.setBounds(86, 132, 72, 18);
		getContentPane().add(lblNewLabel_1);

		areaCode = new RegularTextField();
		areaCode.setRegularText("[0-9]{3,3}");
		areaCode.addKeyListener(new KeyAdapter() {
			
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					prefix.requestFocusInWindow();
				} 
			}
		});
		areaCode.setName("areaCode");
		areaCode.setBounds(153, 39, 86, 24);
		getContentPane().add(areaCode);
		areaCode.setColumns(10);

		prefix = new RegularTextField();
		prefix.setRegularText("[2-9]{3,3}");
		 
		prefix.setName("prefix");
		prefix.setBounds(153, 84, 86, 24);
		getContentPane().add(prefix);
		prefix.setColumns(10);

		suffix = new RegularTextField();
		suffix.setRegularText("[0-9]{4,4}");
		suffix.setName("suffix");
		suffix.setBounds(153, 129, 86, 24);
		getContentPane().add(suffix);
		suffix.setColumns(10);
		
		JLabel lblNewLabel_2 = new JLabel("功能测试3—电话号码问题");
		lblNewLabel_2.setForeground(Color.BLUE);
		lblNewLabel_2.setBounds(31, 13, 200, 18);
		getContentPane().add(lblNewLabel_2);
		
		JLabel lblNewLabel_3 = new JLabel("V3");
		lblNewLabel_3.setFont(new Font("Arial", Font.BOLD, 22));
		lblNewLabel_3.setForeground(Color.RED);
		lblNewLabel_3.setBounds(224, 13, 72, 18);
		getContentPane().add(lblNewLabel_3);

	JButton judgeBtn = new JButton("开始");
	judgeBtn.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
		}
	});
	judgeBtn.addMouseListener(new MouseAdapter() {
		@Override
		public void mouseClicked(MouseEvent arg0) {
			showResult();				
		}

		private void showResult() {
			// TODO Auto-generated method stub
			if(checkAreaCode()==false||checkPrefix()==false||checkSuffix()==false)
			{
				result.setText("结果:不符合！");//显示窗口
				result.setForeground(Color.RED);//设定颜色部分
				result.setVisible(true);//显现窗口
			}
			else
			{
				result.setText("结果：符合！");//显示窗口
				result.setForeground(Color.BLUE);//设定颜色部分
				result.setVisible(true);//显现窗口
			}
		}
	});
	judgeBtn.setName("judgeBtn");
	judgeBtn.setBounds(62, 160, 93, 27);
	getContentPane().add(judgeBtn);
	}
	 
	private boolean checkAreaCode() {
		String code = this.areaCode.getText();
		if (code == null){
			areaCode.setForeground(Color.RED); 
			return false;
		}
		if( code.matches("[0-9]{3,3}")){
			areaCode.setForeground(Color.GREEN);
			return true;
		}
		else{
			areaCode.setForeground(Color.RED); 
			return false;
		} 
	}

	private boolean checkPrefix() {
		String code = this.prefix.getText();
		if (code == null){
			prefix.setForeground(Color.RED); 
			return false;
		}
		if( code.matches("[2-9]{3,3}")){
			prefix.setForeground(Color.GREEN);
			return true;
		}
		else{
			prefix.setForeground(Color.RED); 
			return false;
		} 
	}

	private boolean checkSuffix() {
		String code = this.suffix.getText();
		if (code == null){
			prefix.setForeground(Color.RED); 
			return false;
		}
		if( code.matches("[0-9]{4,4}")){
			prefix.setForeground(Color.GREEN);
			return true;
		}
		else{
			prefix.setForeground(Color.RED); 
			return false;
		} 
	}

	public static void main(String[] args) {

		PhoneNumberFrameV3 frame = new PhoneNumberFrameV3();
		frame.setSize(320, 260);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		
	}
}
