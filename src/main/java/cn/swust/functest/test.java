package cn.swust.functest;

public class test {
	public int fun(int a, int b, int x){
	 	   if((a >= 1) && (b == 0)) 
		        x = x / a;
	 	   if ((a == 2) || (x > 2))
	       x = x + 1;
	 	   return x;  
	 	   }

	public static void main(String[] args) {
		test  t = new test();
//		2,0,4
//		6,5,1
//		2,0,6
//		1,1,1

		System.out.println(t.fun(2,0,4));
		System.out.println(t.fun(6,5,1));
		System.out.println(t.fun(2,0,6));
		System.out.println(t.fun(0,1,1));
	}

}
