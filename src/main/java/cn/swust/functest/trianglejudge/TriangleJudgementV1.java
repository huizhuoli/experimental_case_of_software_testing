package cn.swust.functest.trianglejudge;

import javax.swing.JFrame;
import javax.swing.JLabel; 
import cn.swust.functest.RegularTextField;  
import java.awt.Color;
import java.awt.Font;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent; 
import javax.swing.JButton;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent; 

/**
 * @author 阿文
 *判断给出三边是不是三角形以及其类型
 *1.无法判断等腰三角形。
 *2.只能判断是不是三角形。
 *3.对于输入数据的错误类型无反馈。
 */
public class TriangleJudgementV1 extends JFrame{
	 
	private static final long serialVersionUID = 1L;
	RegularTextField aSide;
	RegularTextField bSide;
	RegularTextField cSide;
	JLabel result;//关于结果的显现
	
	public TriangleJudgementV1() {
	 
		setTitle("功能测试2_三角形问题");
		getContentPane().setLayout(null);// 在标题显现
		
		JLabel label = new JLabel("功能测试1—三角形问题");
		label.setForeground(Color.BLUE);
		label.setBounds(39, 31, 200, 18);
		getContentPane().add(label);//frame 是个窗口, getContentPane是获取窗口的面板, 然后把 label add到面板中
		
		JLabel label_1 = new JLabel("V1");
		label_1.setForeground(Color.RED);
		label_1.setFont(new Font("Arial", Font.BOLD, 22));
		label_1.setBounds(232, 31, 72, 18);
		getContentPane().add(label_1);
		
		JLabel lblA = new JLabel("A边：");
		lblA.setBounds(90, 65, 72, 18);
		getContentPane().add(lblA);
		
		JLabel lblB = new JLabel("B 边：");
		lblB.setBounds(90, 110, 72, 18);
		getContentPane().add(lblB);
		
		JLabel lblC = new JLabel("C 边：");
		lblC.setBounds(90, 155, 72, 18);
		getContentPane().add(lblC);
		//《《《《《《《
		aSide = new RegularTextField();
		aSide.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					bSide.requestFocusInWindow();
				} 
			}
		});
		aSide.setRegularText("");
		aSide.setName("aSide");
		aSide.setColumns(10);
		aSide.setBounds(157, 62, 86, 24);
		getContentPane().add(aSide);
		//《《《《《《
		bSide = new RegularTextField();
		bSide.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {
					cSide.requestFocusInWindow();
				} 
			}
		});
		
		bSide.setRegularText("");
		bSide.setName("bSide");
		bSide.setColumns(10);
		bSide.setBounds(157, 107, 86, 24);
		getContentPane().add(bSide);
		//《《《《《《《
		cSide = new RegularTextField();
		cSide.setRegularText("");
		cSide.setName("cSide");
		cSide.setColumns(10);
		cSide.setBounds(157, 152, 86, 24);
		
		cSide.addKeyListener(new KeyAdapter() {
			@Override
			public void keyPressed(KeyEvent arg0) {
				if (arg0.getKeyCode() == KeyEvent.VK_ENTER) {//回车
					aSide.requestFocusInWindow();//焦点的变换
				} 
			}
		});
		getContentPane().add(cSide); 
		JButton judgeBtn = new JButton("三角形判断");
		judgeBtn.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent arg0) {
				showResult();				
			}
		});
		judgeBtn.setName("judgeBtn");
		judgeBtn.setBounds(288, 84, 113, 27);
		getContentPane().add(judgeBtn);
		
		result = new JLabel("结果：构成三角形");
		result.setVisible(false);
		result.setName("result");
		result.setDisplayedMnemonic(KeyEvent.VK_ENTER);
		result.setBounds(290, 138, 187, 18);
		getContentPane().add(result);
	}
	
	
	public interface TriangleType{//接口
		int ErrorSide = 0;
		int NotTriangle = 1;
		int Triangle = 2;
		int EquilateralTriangle = 4;
	}
	
	private void showResult()
	{
		int iresult = checkSide();//通过这个函数来判断它的实际类型
		switch (iresult)
		{//通过接口来判别类型
		case TriangleType.NotTriangle:
			result.setText("结论：非三角形!");
			result.setForeground(Color.RED);
			result.setVisible(true);//显现窗口
			break;
		case TriangleType.Triangle:
			result.setText("结论：普通三角形!");
			result.setForeground(Color.BLUE);
			result.setVisible(true);//显现窗口
			break;
		}
	}
	
	private int checkSide()
	{
		if (!aSide.availability())//如果不符合规定就直接输出错误
			return TriangleType.ErrorSide;
		if (!bSide.availability())
			return TriangleType.ErrorSide;
		if (!cSide.availability())
			return TriangleType.ErrorSide;
		
		 
		int a, b, c;
		a = Integer.parseInt(aSide.getText());//从区域中拿去数值
		b = Integer.parseInt(bSide.getText());
		c = Integer.parseInt(cSide.getText());
		if ((a + b > c)&&(c + b > a)&&(a + c > b))
				return TriangleType.Triangle;
		else
			return TriangleType.NotTriangle;
	}
	public static void main(String[] args) { 
		TriangleJudgementV1 frame = new TriangleJudgementV1();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setSize(650, 250);
		frame.setVisible(true); 
	}
}
