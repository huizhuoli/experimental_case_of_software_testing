package cn.swust.selenium;

import org.openqa.selenium.By; 
import org.openqa.selenium.WebDriver; 
import org.openqa.selenium.firefox.FirefoxDriver;

public class TestBaidu { 
	public static void main(String[] args) {  
		System.setProperty("webdriver.gecko.driver", "D:\\eclipse-jee-2019-03-R-win32-x86_64\\geckodriver.exe");  

		WebDriver dr = new FirefoxDriver();//ChromeDriver()//HtmlUnitDriver
		dr.get("http://www.baidu.com"); 
		dr.findElement(By.id("kw")).sendKeys("hello Selenium"); 
		dr.findElement(By.id("su")).click(); 
		System.out.println(dr.getTitle());
		try { 
			Thread.sleep(3000); 
		} catch (InterruptedException e) {  
			e.printStackTrace(); 
		} 
		dr.quit(); 
	} 
}

