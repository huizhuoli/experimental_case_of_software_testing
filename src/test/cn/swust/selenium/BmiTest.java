package cn.swust.selenium;

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*; 
import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

 
public class BmiTest {
  
  //浏览器的Driver
  private WebDriver driver;
  
  private String baseUrl;
  
  private boolean acceptNextAlert = true;
  
  private StringBuffer verificationErrors = new StringBuffer();
  
  
  //在每一个测试用例执行之前调用
  @Before
  public void setUp() throws Exception {
	System.out.print("调用public void setUp()");
	System.setProperty("webdriver.gecko.driver", "D:\\geckodriver\\geckodriver.exe");
    System.setProperty("webdriver.firefox.bin", "C:\\Program Files\\Mozilla Firefox\\firefox.exe"); 

    driver = new FirefoxDriver();
    baseUrl = "https://www.google.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void testBmi() throws Exception {
    driver.get("http://jianfei.39.net/box/bmi");
    driver.findElement(By.id("heigth")).click();
    driver.findElement(By.id("heigth")).clear();
    driver.findElement(By.id("heigth")).sendKeys("185");
    driver.findElement(By.id("weight")).click();
    driver.findElement(By.id("weight")).clear();
    driver.findElement(By.id("weight")).sendKeys("70");
    driver.findElement(By.linkText("计 算")).click();
    driver.findElement(By.xpath("//div[5]/div[2]/div[2]")).click();
    try {
      assertEquals("20.5", driver.findElement(By.xpath("//i")).getText());
      
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
  }
  
  
  @Test
  public void testBmi1() throws Exception {
    driver.get("http://jianfei.39.net/box/bmi");
    driver.findElement(By.id("heigth")).click();
    driver.findElement(By.id("heigth")).clear();
    driver.findElement(By.id("heigth")).sendKeys("185");
    driver.findElement(By.id("weight")).click();
    driver.findElement(By.id("weight")).clear();
    driver.findElement(By.id("weight")).sendKeys("70");
    driver.findElement(By.linkText("计 算")).click();
    driver.findElement(By.xpath("//div[5]/div[2]/div[2]")).click();
    try {
      assertEquals("21.5", driver.findElement(By.xpath("//i")).getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
    }
  }
  

  //在每一个测试用例执行结束之后调用
  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
    System.out.print("调用public void tearDown()");
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
