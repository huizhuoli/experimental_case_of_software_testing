package cn.swust.selenium;
 
import java.util.regex.Pattern;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import org.apache.commons.io.FileUtils;
import org.junit.*;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.junit.runners.Parameterized.Parameters;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.support.ui.Select;

@RunWith(Parameterized.class) 
public class MyBmiTest {
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();
  
  private String heitht;
  private String weight;
  private String expBmi;
  
  public MyBmiTest(String height, String weight, String expBmi) {
	  this.heitht = height;
	  this.weight = weight;
	  this.expBmi = expBmi; 
  }
  
  @Parameters 
  public static Collection data() { 
	  return Arrays.asList(new Object[][] {{"185", "70","20.5"},{"185", "75","20.5"},{"185", "70","21.5"},{"189", "70","21.5"}}); 
  } 
  

  @Before
  public void setUp() throws Exception {
	System.setProperty("webdriver.gecko.driver", "D:\\geckodriver\\geckodriver.exe");
	System.setProperty("webdriver.firefox.bin", "C:\\Program Files\\Mozilla Firefox\\firefox.exe"); 
	
    driver = new FirefoxDriver();
    baseUrl = "https://www.google.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
    

  }

  @Test
  public void testBmi() throws Exception {
    driver.get("http://jianfei.39.net/box/bmi");
    driver.findElement(By.id("heigth")).click();
    driver.findElement(By.id("heigth")).clear();
    driver.findElement(By.id("heigth")).sendKeys(this.heitht);
    driver.findElement(By.id("weight")).click();
    driver.findElement(By.id("weight")).clear();
    driver.findElement(By.id("weight")).sendKeys(this.weight);
    driver.findElement(By.linkText("计 算")).click();
    driver.findElement(By.xpath("//p")).click();
    try {
      assertEquals(this.expBmi, driver.findElement(By.xpath("//i")).getText());
    } catch (Error e) {
      verificationErrors.append(e.toString());
      saveScreenShot();
    }
  }
  

  @After
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }
  
	public void saveScreenShot() throws IOException {
		SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd_HHmmss");
		String preFileName = df.format(new Date());
		File screenShot = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);

		String fileName = "d:/" + preFileName + "_bmi.jpg";
		FileUtils.copyFile(screenShot, new File(fileName));
	}

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}

