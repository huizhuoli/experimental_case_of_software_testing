package cn.swust.selenium;
 

import java.util.regex.Pattern;
import java.util.concurrent.TimeUnit;
import org.junit.*;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;

import static org.junit.Assert.*;
import static org.hamcrest.CoreMatchers.*;
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver; 

public class  PermutationTest{
  private WebDriver driver;
  private String baseUrl;
  private boolean acceptNextAlert = true;
  private StringBuffer verificationErrors = new StringBuffer();

  @BeforeEach
  public void setUp() throws Exception {
    driver = new FirefoxDriver();
    baseUrl = "https://www.katalon.com/";
    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
  }

  @Test
  public void test() throws Exception {
    driver.get("http://www.jisuanqinet.com/shuxue/pailiezuhe.html");
    driver.findElement(By.name("nCombo")).click();
    driver.findElement(By.name("nCombo")).clear();
    driver.findElement(By.name("nCombo")).sendKeys("3");
    driver.findElement(By.name("rCombo")).click();
    driver.findElement(By.name("rCombo")).clear();
    driver.findElement(By.name("rCombo")).sendKeys("3");
    driver.findElement(By.xpath("//input[@value='计算']")).click();
    driver.findElement(By.xpath("(//input[@name='nCombo'])[2]")).click();
    driver.findElement(By.xpath("(//input[@name='nCombo'])[2]")).clear();
    driver.findElement(By.xpath("(//input[@name='nCombo'])[2]")).sendKeys("3");
    driver.findElement(By.xpath("(//input[@name='rCombo'])[2]")).click();
    driver.findElement(By.xpath("(//input[@name='rCombo'])[2]")).clear();
    driver.findElement(By.xpath("(//input[@name='rCombo'])[2]")).sendKeys("3");
    driver.findElement(By.xpath("(//input[@value='计算'])[2]")).click();
    driver.findElement(By.xpath("(//input[@name='displayAnswer'])[2]")).click();
  }

  @AfterEach
  public void tearDown() throws Exception {
    driver.quit();
    String verificationErrorString = verificationErrors.toString();
    if (!"".equals(verificationErrorString)) {
      fail(verificationErrorString);
    }
  }

  private boolean isElementPresent(By by) {
    try {
      driver.findElement(by);
      return true;
    } catch (NoSuchElementException e) {
      return false;
    }
  }

  private boolean isAlertPresent() {
    try {
      driver.switchTo().alert();
      return true;
    } catch (NoAlertPresentException e) {
      return false;
    }
  }

  private String closeAlertAndGetItsText() {
    try {
      Alert alert = driver.switchTo().alert();
      String alertText = alert.getText();
      if (acceptNextAlert) {
        alert.accept();
      } else {
        alert.dismiss();
      }
      return alertText;
    } finally {
      acceptNextAlert = true;
    }
  }
}
