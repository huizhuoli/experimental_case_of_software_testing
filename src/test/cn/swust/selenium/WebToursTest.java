package cn.swust.selenium;
  
import java.util.concurrent.TimeUnit; 
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.Assert.*; 
import org.openqa.selenium.*;
import org.openqa.selenium.firefox.FirefoxDriver; 

public class WebToursTest {
	  private String page;
	  private WebDriver driver;
	  private String baseUrl;
	  private boolean acceptNextAlert = true;
	  private StringBuffer verificationErrors = new StringBuffer();

	  @BeforeEach
	  public void setUp() throws Exception {
	    driver = new FirefoxDriver();
	    baseUrl = "http://127.0.0.1:9080/WebTours/";
	    driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
	  }

	  
	  @Test
	  public void testLoginOut()throws Exception{
		  login();
		  
		  logout();
	  }
	  public void logout() throws Exception { 
		  driver.switchTo().parentFrame();  
		  page = driver.getPageSource();
		  System.out.println(page); 
		  driver.switchTo().frame("navbar"); 
		  page = driver.getPageSource();
		  System.out.println(page);
		    // ERROR: Caught exception [ERROR: Unsupported command [selectFrame | relative=parent | ]]
		    // ERROR: Caught exception [ERROR: Unsupported command [selectFrame | index=0 | ]]
		  driver.findElement(By.xpath("//img[@alt='SignOff Button']")).click();
    	  driver.switchTo().frame(1); 
    	  driver.switchTo().frame("body");  
		  driver.switchTo().frame("info");  
		  page = driver.getPageSource();
		  System.out.println(page);
		  try {
		      assertEquals("sign up now", driver.findElement(By.xpath("//a/b")).getText());
		    } catch (Error e) {
		      verificationErrors.append(e.toString());
		    } 
	  }
	
	
	  public void login() throws Exception {
		driver.get(baseUrl);
	    // ERROR: Caught exception [ERROR: Unsupported command [selectFrame | index=1 | ]]
	    // ERROR: Caught exception [ERROR: Unsupported command [selectFrame | index=0 | ]]
	    driver.switchTo().frame("body");
	    driver.switchTo().frame("navbar");  
	    driver.findElement(By.name("username")).click();
	    driver.findElement(By.name("password")).clear();
	    driver.findElement(By.name("password")).sendKeys("bean");
	    driver.findElement(By.name("username")).clear();
	    driver.findElement(By.name("username")).sendKeys("jojo");
	    driver.findElement(By.name("password")).click();
	    driver.findElement(By.name("login")).click();
	   
	    driver.switchTo().frame(1);  
	    driver.switchTo().frame("body");
	    driver.switchTo().frame("info");  
	    page = driver.getPageSource();
	    System.out.println(page);
	    // ERROR: Caught exception [ERROR: Unsupported command [selectFrame | relative=parent | ]]
	    // ERROR: Caught exception [ERROR: Unsupported command [selectFrame | index=1 | ]]
	    try {
	    	String name = driver.findElement(By.xpath("//b")).getText();
	      assertEquals("jojo", name);
	    } catch (Error e) {
	      verificationErrors.append(e.toString());
	    }  
	  }

	  @AfterEach
	  public void tearDown() throws Exception {
	    driver.quit();
	    String verificationErrorString = verificationErrors.toString();
	    if (!"".equals(verificationErrorString)) {
	      fail(verificationErrorString);
	    }
	  }

	  private boolean isElementPresent(By by) {
	    try {
	      driver.findElement(by);
	      return true;
	    } catch (NoSuchElementException e) {
	      return false;
	    }
	  }

	  private boolean isAlertPresent() {
	    try {
	      driver.switchTo().alert();
	      return true;
	    } catch (NoAlertPresentException e) {
	      return false;
	    }
	  }

	  private String closeAlertAndGetItsText() {
	    try {
	      Alert alert = driver.switchTo().alert();
	      String alertText = alert.getText();
	      if (acceptNextAlert) {
	        alert.accept();
	      } else {
	        alert.dismiss();
	      }
	      return alertText;
	    } finally {
	      acceptNextAlert = true;
	    }
	  }
	}
